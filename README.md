# Digital Pages Challenge

### Antes de tudo
Essa aplicação foi desenvolvida usando a mais recente versão do xCode(7.2) e depende do uso do Pods para gerenciar dependências.

### CocoaPods
CocoaPods é o gerenciador de depedência para projetos Objective-C e Swift. Mais informações em [CocoaPods]

   [CocoaPods]: <https://cocoapods.org/>

### Como instalar

* Atualize sua RubyGems para a versão mais recente.

```sh
$ gem update --system
```

* Instale o CocoaPods

```sh
$ gem install cocoapods
```

* Execute Pod Install

```sh
$ gem pod install
```

After all these steps you might see something like this:

  **[!] A partir de agora use `Digital Pages Challenge.xcworkspace`.**
A partir de agora você deve usar Digital Pages Challenge.xcworkspace instead Digital Pages Challenge.xcodeproj
