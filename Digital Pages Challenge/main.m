//
//  main.m
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/15/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
