//
//  AppDelegate.h
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/15/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

