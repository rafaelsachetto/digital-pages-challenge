//
//  DCShotDetailViewController.h
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/16/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shot.h"

@interface DCShotDetailViewController : UIViewController

@property (strong, nonatomic) Shot *shot;

@end
