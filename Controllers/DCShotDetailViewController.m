//
//  DCShotDetailViewController.m
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/16/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import "DCShotDetailViewController.h"
#import "UIImageView+AFNetworking.h"

@interface DCShotDetailViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *shotImageView;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *likesLabel;
@property (strong, nonatomic) IBOutlet UIWebView *detailWebView;

@end

@implementation DCShotDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = self.shot.title;
    if ([self.shot.image class] != [NSNull class]) {
        
        [self.shotImageView setImageWithURL:[NSURL URLWithString:self.shot.image] placeholderImage:nil];
    }
    if ([self.shot.avatar class] != [NSNull class]) {
    [self.avatarImageView setImageWithURL:[NSURL URLWithString:self.shot.avatar] placeholderImage:nil];
    }
    self.likesLabel.text = [NSString stringWithFormat:@"❤︎ %li", self.shot.likes];
    [self.detailWebView loadHTMLString:[self prepareDetailWithContent:self.shot.detail] baseURL:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)prepareDetailWithContent:(NSString *)content {
    NSMutableString *detail = NSMutableString.new;
    if ([content class] == [NSNull class]) {
        content = @"<p>Shot sem descrição</p>";
    }
    [detail appendString:@"<!DOCTYPE html>"];
    [detail appendFormat:@"<html><head>%@</head>", @""];
    [detail appendFormat:@"<body>%@</body></html>", content];

    return detail;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
