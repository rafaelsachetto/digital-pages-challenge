//
//  DCShotTableViewController.m
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/16/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import "DCShotTableViewController.h"
#import "DCShotTableViewCell.h"
#import "DCShotDetailViewController.h"
#import "DCAPIServices.h"
#import "Shot.h"
#import "UIImageView+AFNetworking.h"
#import "Reachability.h"

#define kCellIdentifier @"ShotTableViewCell"
#define kPresentShotDetail @"presentShotDetail"

@interface DCShotTableViewController () <UIScrollViewDelegate, UIAlertViewDelegate>

@property Shot *shot;
@property NSMutableArray *content;
@property NSUInteger *page;
@property BOOL isLoadingNextPage;

@end

@implementation DCShotTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Reachability *internetConnection = [Reachability reachabilityForInternetConnection];
    [internetConnection startNotifier];
    
    if ([internetConnection currentReachabilityStatus] == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet indisponível" message:@"Não foi possível encontrar uma conexão" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        Reachability *hostConnection = [Reachability reachabilityWithHostName:@"www.dribble.com"];
        [hostConnection startNotifier];
        if ([hostConnection currentReachabilityStatus] == NotReachable) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"API indisponível" message:@"A API está indisponível no momento" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    self.tableView.delegate = self;
    
    [self getShots];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Get Shots
-(void)getShots {
    DCAPIServices *service = [DCAPIServices sharedInstance];
    
    [service getShotsWithPage:1 success:^(id result) {
        self.page = 1;
        self.content = [NSMutableArray arrayWithArray:result];
        
        if (self.content) {
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        NSLog(@"e: %@", error);
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.content.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DCShotTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    Shot *shot = [self.content objectAtIndex:indexPath.row];
    
    if ([shot.image class] != [NSNull class]) {
        [cell.shotImageView setImageWithURL:[NSURL URLWithString:shot.image] placeholderImage:nil];
    }
    if ([shot.avatar class] != [NSNull class]) {
        [cell.avatarImageView setImageWithURL:[NSURL URLWithString:shot.avatar] placeholderImage:nil];
    }
    cell.likesLabel.text = [NSString stringWithFormat:@"❤︎ %li", shot.likes];
    cell.titleLabel.text = shot.title;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.shot = [self.content objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:kPresentShotDetail sender:nil];
    
}

#pragma mark - Scroll

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSLog(@"y: %li xpt: %li", (long)scrollView.contentOffset.y, (self.content.count - 3) * 220);
    if (scrollView.contentOffset.y >= (self.content.count - 3) * 220 && !self.isLoadingNextPage) {
        self.isLoadingNextPage = YES;
        
        [[DCAPIServices sharedInstance] getShotsWithPage:self.page + 1 success:^(id result) {
            if ([result count] > 0) {
                self.page++;
                self.content = [self.content mutableCopy];
                [self.content addObjectsFromArray:result];
                [self.tableView reloadData];
            }
            self.isLoadingNextPage = false;
        } failure:^(NSError *error) {
            self.isLoadingNextPage = false;
            NSLog(@"e: %@", error);
        }];
    }

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:kPresentShotDetail]) {
        DCShotDetailViewController *shotDetailViewController = segue.destinationViewController;
        
        shotDetailViewController.shot = self.shot;
    }
}

@end
