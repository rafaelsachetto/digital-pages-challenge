//
//  Shots.m
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/16/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import "Shot.h"

@implementation Shot

-(id)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        self.avatar = [[dictionary valueForKey:@"user"] valueForKey:@"avatar_url"];
        self.image = [[dictionary valueForKey:@"images"] valueForKey:@"hidpi"];
        self.title = [dictionary valueForKey:@"title"];
        self.detail = [dictionary valueForKey:@"description"];
        self.likes = [[dictionary valueForKey:@"likes_count"] integerValue];
    }

    return self;
}

@end
