//
//  Shots.h
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/16/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Shot : NSObject

@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *detail;
@property NSUInteger *likes;

-(id)initWithDictionary:(NSDictionary *)dictionary;

@end
