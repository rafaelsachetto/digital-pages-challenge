//
//  APIServices.h
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/16/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^APIRequestSuccessCompletion)(id result);
typedef void(^APIRequestFailureCompletion)(NSError *error);

#define BASE_URL @"https://api.dribbble.com/v1/"
#define ACCESS_TOKEN @"243c436dd79c8b54276ace5b1c9311b12415aa438e23388fa6000798fa2f24a4"
#define SHOT_URL @"shots?sort=views"

@interface DCAPIServices : NSObject

+(instancetype)sharedInstance;

-(void)getShotsWithPage:(NSUInteger *)page success:(APIRequestSuccessCompletion)success failure:(APIRequestFailureCompletion)failure;

@end
