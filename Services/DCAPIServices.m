//
//  APIServices.m
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/16/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import "DCAPIServices.h"
#import <AFNetworking/AFNetworking.h>
#import "Shot.h"

#define BASE_URL @"https://api.dribbble.com/v1/"
#define ACCESS_TOKEN @"243c436dd79c8b54276ace5b1c9311b12415aa438e23388fa6000798fa2f24a4"
#define SHOT_URL @"shots?sort=views"
#define kShotPerPage @(6)

@implementation DCAPIServices

+(instancetype)sharedInstance {
    static DCAPIServices *shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = self.new;
    });
    
    return shared;
}

-(AFHTTPSessionManager *)manager {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    return manager;
}


-(void)getShotsWithPage:(NSUInteger *)page success:(APIRequestSuccessCompletion)success failure:(APIRequestFailureCompletion)failure {
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_URL, SHOT_URL];
    
    AFHTTPSessionManager *manager = [self manager];
    
    [manager GET:url
      parameters:@{
                                  @"sort": @"views",
                                  @"access_token": ACCESS_TOKEN,
                                  @"per_page": kShotPerPage,
                                  @"page": [NSString stringWithFormat:@"%li", (long)page]
                  }
        progress:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             NSMutableArray *array = ((NSMutableArray *)responseObject);
             
             NSMutableArray *content = [NSMutableArray array];
             
             for (NSDictionary *dict in array)
                 [content addObject: [[Shot alloc] initWithDictionary: dict]];
             
             success(content);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failure(error);
         }];
}

@end
