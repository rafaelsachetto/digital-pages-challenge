//
//  DCShotTableViewCell.h
//  Digital Pages Challenge
//
//  Created by Rafael Sachetto on 12/16/15.
//  Copyright © 2015 Rafael Sachetto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCShotTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *shotImageView;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *likesLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
